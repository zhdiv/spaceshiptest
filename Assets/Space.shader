﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Space"
{
	Properties
	{
		_PlanetColor("Planet color", Color) = (1,1,1,1)
	}
		SubShader
	{
		Pass
	{
		Tags { "Queue" = "Opaque" "RenderType" = "Opaque" }
		LOD 200

		ZWrite Off
		//Blend SrcAlpha OneMinusSrcAlpha


		CGPROGRAM
		#include "SimplexNoise3D.hlsl"
		#include "ClassicNoise3D.hlsl"
		#pragma target 3.0
		#pragma vertex vert
		#pragma fragment frag



		uniform float3 _Position;
		uniform float4 _PlanetColor;

		uniform float _Seed;
		uniform float _SpecialMode = 0.0;
		uniform float _CmprIndexMin;
		uniform float _CmprIndexMax;

		struct appdata {
			float4 vertex : POSITION;

		};

		struct v2f {
			float4 vertex : SV_POSITION;
			float3 worldPos : TEXCOORD0;
		};

		v2f vert(appdata v) {
			v2f o;

			o.worldPos = mul(unity_ObjectToWorld, v.vertex);
			o.vertex = UnityObjectToClipPos(v.vertex);

			return o;
		}

		float size = 10;

		float remap(float value, float low1, float high1, float low2, float high2)
		{
			return low2 + (value - low1) * (high2 - low2) / (high1 - low1);
		}

		fixed4 frag(v2f i) : SV_Target{

			float2 uv = i.worldPos - float2(0.5, 0.5);
			float4 result = float4(0.0, 0.0, 0.0, 0.0);
			float3 newuv = float3(ceil(uv), cos(3.14));
			float c = snoise(newuv);
			float initialc = float(c);

			float mapSC = 0;
			if (c >= _CmprIndexMin && c <= _CmprIndexMax)
			{
				mapSC = 1;
			}

			c = lerp(c, mapSC, _SpecialMode);
			float row = frac(uv.x);
			float col = frac(uv.y);

			float2 coords = float2(row - 0.5, col - 0.5);

			result.r = c;
			result.g = c;
			result.b = initialc;
			result.a = 1;
			return result;
   }

	ENDCG
	}
	}


		FallBack "Diffuse"
}
