﻿using System;
using System.Collections;
using UnityEngine;

public class DesktopInputManager : MonoBehaviour
{
    public static event Action<int, int> OnMove;
    public static event Action<int> OnZoomChange;

    public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
    public float orthoZoomSpeed = 0.01f;        // The rate of change of the orthographic size in orthographic mode.

    public void MoveLR(int dir)
    {
        StartActions(() =>
        {
            if (Mathf.Abs(dir) > 0)
                OnMove?.Invoke(dir, 0);
        });
    }

    public void MoveUD(int dir)
    {
        StartActions(() =>
        {
            if (Mathf.Abs(dir) > 0)
                OnMove?.Invoke(0, dir);
        });
    }

    public void DoZoom(int delta)
    {
        StartActions(() =>
       {
           if (Mathf.Abs(delta) > 0)
               OnZoomChange?.Invoke(delta);
       });
    }

    public void StopAction()
    {
        if (selectedAction != null)
        {
            StopCoroutine(selectedAction);
            selectedAction = null;
        }
    }

    void StartActions(Action act)
    {
        if (selectedAction != null)
        {
            StopCoroutine(selectedAction);
            selectedAction = null;
        }
        selectedAction = StartCoroutine(ActionCoroutine(act));
    }

    Coroutine selectedAction;

    IEnumerator ActionCoroutine(Action act)
    {
        while (true)
        {
            act?.Invoke();
            yield return null;
            //yield return new WaitForSeconds(0.2f);
        }
    }


    void Update()
    {

        var u = Input.GetKey(KeyCode.W) ? 1 : 0;
        var d = Input.GetKey(KeyCode.S) ? -1 : 0;
        var l = Input.GetKey(KeyCode.A) ? -1 : 0;
        var r = Input.GetKey(KeyCode.D) ? 1 : 0;

        var v = u + d;
        var h = l + r;

        if (Mathf.Abs(v) > 0 || Mathf.Abs(h) > 0)
            OnMove?.Invoke(h, v);


        float zoom = Input.mouseScrollDelta.y;

        if (Mathf.Abs(zoom) > 0)
            OnZoomChange?.Invoke(Mathf.RoundToInt(zoom + Mathf.Sign(zoom) * 0.5f));

    }
}
