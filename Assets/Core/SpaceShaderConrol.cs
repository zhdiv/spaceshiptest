﻿using UnityEngine;
using UnityEngine.UI;

public class SpaceShaderConrol : MonoBehaviour
{
    [SerializeField]
    private Material mtl;

    [SerializeField]
    private float seed = 0;

    [SerializeField]
    public static float indexCmp;

    [SerializeField]
    private float indexCmpDiff = 20;

    [SerializeField]
    [Range(0, 10000)]
    private float maxIndex = 10000;

    [SerializeField]
    private Text playerIndexText;

    public static bool isSpecialMode = false;

    public void ChangeIndex(float v)
    {
        indexCmp = v;
        UpdateParams();
        playerIndexText.text = ((int)v).ToString();
    }


    void Start()
    {
        mtl.SetFloat("_Seed", seed);
        isSpecialMode = false;
        indexCmp = Random.Range(100, maxIndex);
        playerIndexText.text = ((int)indexCmp).ToString();
    }

    [ContextMenu("UpdateSeed")]
    public void UpdateSeed()
    {
        mtl.SetFloat("_Seed", seed);
    }

    [ContextMenu("UpdateParams")]
    public void UpdateParams()
    {
        mtl.SetFloat("_CmprIndexMin", Mathf.Clamp((indexCmp - indexCmpDiff) / maxIndex, 0, 1));//0-1 remapped
        mtl.SetFloat("_CmprIndexMax", Mathf.Clamp((indexCmp + indexCmpDiff) / maxIndex, 0, 1));
    }

    public void ToggleSpectialMode(float v)
    {
        isSpecialMode = v > 0;
        mtl.SetFloat("_SpecialMode", v);

        mtl.SetFloat("_CmprIndexMin", Mathf.Clamp((indexCmp - indexCmpDiff) / maxIndex, 0, 1));//0-1 remapped
        mtl.SetFloat("_CmprIndexMax", Mathf.Clamp((indexCmp + indexCmpDiff) / maxIndex, 0, 1));
    }
}
