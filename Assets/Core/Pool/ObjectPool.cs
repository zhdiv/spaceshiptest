﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface PoolObject
{
    void ReturnToPool();
    bool IsActive();
}

public class PoolBase : MonoBehaviour, PoolObject
{
    public bool IsActive()
    {
        return gameObject.activeInHierarchy;
    }

    public void ReturnToPool()
    {
        //Debug.Log("Return to pool"); ;
        gameObject.SetActive(false);
    }
}


public class ObjectPool
{
    List<PoolBase> objects;
    Transform objectsParent;

    public void Initialize(int count, PoolBase sample, Transform objects_parent)
    {
        objects = new List<PoolBase>();
        objectsParent = objects_parent;
        for (int i = 0; i < count; i++)
        {
            AddObject(sample, objects_parent);
        }
    }

    public PoolBase GetObject()
    {
        for (int i = 0; i < objects.Count; i++)
        {
            if (objects[i].IsActive() == false)
            {
                return objects[i];
            }
        }
        AddObject(objects[0], objectsParent);
        return objects[objects.Count - 1];
    }

    void AddObject(PoolBase sample, Transform objects_parent)
    {
        GameObject temp;
        temp = GameObject.Instantiate(sample.gameObject);
        temp.name = sample.name;
        temp.transform.SetParent(objects_parent);
        objects.Add(temp.GetComponent<PoolBase>());

        temp.SetActive(false);
    }
   

}
