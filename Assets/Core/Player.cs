﻿using System;
using UnityEngine;
public class Player : MonoBehaviour
{
    static int posx, posy;

    const float inputdelay = 0.12f;
    private float endtime = 0;

    public static Vector3Int GetPos()
    {
        return new Vector3Int(posx, posy, 0);
    }


    ToTexture redrawElemTexture;

    private void OnMouseDown()
    {
        var random = UnityEngine.Random.Range(0, 10000);
        FindObjectOfType<SpaceShaderConrol>().ChangeIndex(random);
    }

    void Awake()
    {
        redrawElemTexture = FindObjectOfType<ToTexture>();
        DesktopInputManager.OnMove += UpdatePosition;
    }

    public void UpdatePosition(int deltax, int deltay)
    {
        if (Time.time >= endtime)
        {
            posx += deltax;
            posy += deltay;
            transform.position = new Vector3(posx, posy, 0);
            endtime = Time.time + inputdelay;
        }
        redrawElemTexture.UpdateObjects();
    }
}
