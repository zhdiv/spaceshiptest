﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour
{

    int minZoom = 5;
    [SerializeField]
    public static int zoom = 5;

    [SerializeField]
    private Camera cam;
    [SerializeField]
    private Camera rtcam;

    [SerializeField]
    SpaceShaderConrol shaderControl;
    [SerializeField]
    private Transform quad;

    [SerializeField]
    private int ZOOM_TO_CMP_VIEW_MODE = 50;

    ToTexture redrawElemTexture;

    void Awake()
    {
        redrawElemTexture = FindObjectOfType<ToTexture>();
        //cam.aspect = 1;
        shaderControl.ToggleSpectialMode(0f);
        DesktopInputManager.OnZoomChange += DesktopInputManager_OnZoomChange;
        SetZoom(zoom);
    }

    private void SetZoom(int size)
    {
        cam.orthographicSize = size * 0.5f;
        rtcam.orthographicSize = cam.orthographicSize;
        quad.transform.localScale = Vector3.one * size * 2;
    }

    bool specialModeTriggered = false;

    const float inputdelay = 0.05f;
    private float endtime = 0;

    private void DesktopInputManager_OnZoomChange(int delta)
    {
        if (Time.time >= endtime)
        {

            zoom += delta;
            zoom = Mathf.Max(zoom, minZoom);
            SetZoom(zoom);
            if (zoom >= ZOOM_TO_CMP_VIEW_MODE * 2)
            {
                if (!specialModeTriggered)
                {
                    shaderControl.ToggleSpectialMode(1f);
                    specialModeTriggered = true;
                }
            }
            else
            {
                if (specialModeTriggered)
                {
                    shaderControl.ToggleSpectialMode(0f);
                    specialModeTriggered = false;
                }
            }
            endtime = Time.time + inputdelay;
        }
        redrawElemTexture.UpdateObjects();
        //Debug.Log(zoom);
    }

}
