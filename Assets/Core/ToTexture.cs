﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToTexture : MonoBehaviour
{
    public Camera renderCam;

    public Texture2D tex;

    public Color[] pixels;

    public bool spawnObject = true;

    [SerializeField]
    GUIStyle skin;
    private void Start()
    {
    }

    public void UpdateSpawnObject(bool v)
    {
        spawnObject = v;
    }

    void LateUpdate()
    {
        //if (CamControl.zoom > 100) return;

        int w = CamControl.zoom, h = CamControl.zoom;
        tex = new Texture2D(w, h, TextureFormat.RGBA32, false);
        Camera activeCamera = renderCam;

        RenderTexture rt =  RenderTexture.GetTemporary(w, h, 24);
        activeCamera.targetTexture = rt;
        activeCamera.Render();

        RenderTexture.active = rt;
        tex.ReadPixels(new Rect(0,0, w, h), 0, 0);
        pixels=tex.GetPixels(0, 0, w, h, 0);
        //tex.Apply();
        // Clean up
        activeCamera.targetTexture = null;
        RenderTexture.active = null; // added to avoid errors 
        RenderTexture.ReleaseTemporary(rt);
    }

    public Rect pixelToScreen(int i)//todo fix pos
    {
        float pos = Screen.width - Screen.height;
        float offset = pos / 2;
        var y = (Screen.height - (Screen.height / CamControl.zoom)) - (i / CamControl.zoom) * (Screen.height / CamControl.zoom);
        var x = offset + (i % CamControl.zoom) * (Screen.height / CamControl.zoom);
        return new Rect(x, y, 40, 15);
    }

    public Vector3 getWorldPoint(int i)
    {
        var p = Player.GetPos();
        Debug.Log($"x = i%Cam.zoom = {i % CamControl.zoom}");
        Debug.Log($"y = i/Cam.zoom = {i / CamControl.zoom}");
        var x = (i % CamControl.zoom) + p.x -  CamControl.zoom/2;
        var y = (i / CamControl.zoom) + p.y - CamControl.zoom/2;
        return new Vector3(x, y, 0);
    }

    private void OnGUI()
    {
        for(int i=0; i < pixels.Length; i++)//
        {
            if (pixels[i].r > 0)//pixel in r, value in b
            {
                var pos = pixelToScreen(i);
                //GUI.Box(pos, "");
                if (SpaceShaderConrol.isSpecialMode)
                {
                    GUI.Box(pos, ((int)(pixels[i].b*10000)).ToString(), skin);
                }
                else
                    GUI.Box(pos, ((int)(pixels[i].b * 10000)).ToString(), skin);
            }            
        }
    }

    List<SpaceObject> objects = new List<SpaceObject>();

    Coroutine c;

    public void UpdateObjects()
    {
        if (!spawnObject)
        {
            if (c != null)
                StopCoroutine(c);
            c = null;
            return;
        }
        //Delayed redraw
        if(c!=null)
            StopCoroutine(c);
        c = StartCoroutine(UpdateObj());
    }

    IEnumerator UpdateObj()
    {

        yield return null;

        foreach (var o in objects)
        {
            o.ReturnToPool();
        }
        objects.Clear();

        for (int i = 0; i < pixels.Length; i++)//
        {
            if (pixels[i].r > 0)//pixel in r, value in b
            {
                if (!SpaceShaderConrol.isSpecialMode)
                {
                    var obj = PoolManager.GetObject("Planet", getWorldPoint(i), Quaternion.identity).GetComponent<SpaceObject>();
                    obj.Raiting = Mathf.RoundToInt(pixels[i].b * 10000);
                    objects.Add(obj);
                }
            }
        }
        yield break;
    }
}

