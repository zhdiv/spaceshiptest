﻿using UnityEngine;

public class SpaceObject : PoolBase
{
    public int Raiting = 0;

    public const int MAX_RAITING = 10000;

    public void Init(int x, int y, int r)
    {
        transform.position = new Vector3Int(x, y, 0);
        Raiting = r;
    }
}
